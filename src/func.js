const getSum = (str1, str2) => {
  if(isNaN(Number(str1)) || isNaN(Number(str2)) || Number(str1) + Number(str2) == 0){
    return false;
  }
  return String(Number(str1) + Number(str2));
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var counterPost = 0;
  var counterComment = 0;

  listOfPosts.forEach(a =>{

    var key = Object.keys(a);

    for(var i = 0; i < key.length; i++){
      if(key[i] == 'author' && Object.values(a)[i] == authorName){
        counterPost++;
      }
    }

    for(var i = 0; i < key.length; i++){
      if(key[i] == 'comments'){
        var objInKey = Object.values(a)[i];
        objInKey.forEach(b=>{

          var keyInKey = Object.keys(b);

          for(var j = 0; j < keyInKey.length; j++){
            if(keyInKey[j] == 'author' && Object.values(b)[j] == authorName){
              counterComment++;
            }
          }
        })
      }
    }
    })
    return "Post:" + counterPost + ',comments:' + counterComment;
}

const tickets=(people)=> {

  var Vasiamoney = 0;
  var bool = 'YES';

  people.forEach(person => {
      if(Number(person) - 25 > Number(Vasiamoney)){
        bool = 'NO';
      }
      Vasiamoney += 25;
    });


    return bool;
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
